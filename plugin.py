from modules.base_plugin import BasePlugin


class SurePlugin(BasePlugin):
    """
    This is a demo plugin
    """

    __version__ = "0.0.1"

    _option1 = None

    @property
    def name(self):
        return "Example"

    @property
    def command(self):
        return "demo"

    @property
    def options(self):
        return [
            ('Option1', self._option1)
        ]

    def do_option1(self, args: str):
        """
Set the value of Option1
"""
        self._option1 = args

    def run(self, args):
        """
This module adds sample vulnerabilities.
"""
        self.add_vuln_by_url("SC-9999", "Demo URL added", "https://www.surecloud.com")
        self.add_vuln_by_ip("SC-9998", "Demo IP added", "127.0.0.1", "tcp", 445)
        self.add_vuln_by_hostname("SC-9997", "Demo Hostname added", "www.surecloud.com", "tcp", 80)




